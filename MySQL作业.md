- **搭建一个MySQL高可用架构集群环境（******4台主机，******1主、2从、1 MHA）**

1. 首先实现一主两从的同步复制功能（采用半同步复制机制）

1. 然后采用MHA实现主机出故障，从库能自动切换功能

1. MHA高可用搭建后，在主库新建商品表进行效果测试

1. 作业需要提交集群环境搭建手册和效果演示视频手册：包含环境软件版本和架构介绍、环境安装过程、操作的问题和注意事项等。视频：仅录制环境介绍和效果演示。

----------------------------------------------------------------------------

作业资料说明：

1、提供资料：说明文档、验证及讲解视频。

2、讲解内容包含：题目分析、实现思路、环境介绍。

3、说明文档包含：

l 环境软件版本、架构介绍

l 环境安装过程（各个配置加注释）

l 操作过程中遇到的问题

l 操作注意事项

4、效果视频验证：

l 集群环境

* 4台

* 1主，2从，1MHA

一主两从的半同步复制功能

MHA实现主机出故障，从库能自动切换功能

l 环境介绍

 * 介绍涉及的各个软件的版本

 * 介绍各个机器对应角色&作用&ip地址

l 主库新建商品表

l 添加数据，演示半同步复制

    * 先查询主从库数据

    * 添加后，再次查询主从库数据

    * 查看log，是否显示半同步

l 主机出故障，从库能自动切换功能

  * 关闭 主服务器，查看MHA服务器是否正常切换

App1:mysql Master failower 之前主ip(之前主ip:3306) to 切换后ip(切换后ip:3306) succeeded

  * 在切换后的主节点机器上查看状态展示效果

  * 在切换后的主节点机器数据库添加数据，分别查询主从库数据一致

------------------------------------------------------------------------------------------------------------

## 一. 环境及软件版本

| 环境   | 版本            |
| ---- | ------------- |
| 操作系统 | centos 7      |
| 虚拟机  | WMware 15.5   |
| 远程连接 | XShell 6.0    |
| 数据库  | mysql  5.7.28 |

机器对应角色：

![](https://tcs.teambition.net/storage/311zcdef362bb5979747f58cdd48253f254c?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkzMDE2NSwiaWF0IjoxNjA2MzI1MzY1LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXpjZGVmMzYyYmI1OTc5NzQ3ZjU4Y2RkNDgyNTNmMjU0YyJ9.RAG_KdlD2cnAWjKH8nA2AKS5vzpxzr1-yTljM7-YauY&download=image.png "")

## 二. 环境准备

### 1. 安装mysql

1. 从官网下载mysql 5.7.28

1. 安装mysql包括common 、libs、libs-compat、client、server包。

1. 安装mysql

1. 初始化数据库及密码

1. 3台机器都需要按照以上步骤进行安装

### 2. mysql主从配置

**主库配置**

1. 主库配置主要是开启binlog日志及配置server-id

修改/etc/my.cnf.

![](https://tcs.teambition.net/storage/311z474b4862be154e3dec240bdf338a27c0?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyNzEzNSwiaWF0IjoxNjA2MzIyMzM1LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXo0NzRiNDg2MmJlMTU0ZTNkZWMyNDBiZGYzMzhhMjdjMCJ9.6z-IC-KX4IxbyWkLdfRz19o3HjW0MjBpqF6pQbADIvw&download=image.png "")

1. 保存重启数据库

1. 登录master，给从库进行授权操作。

![](https://tcs.teambition.net/storage/311z494c2ce6be9426d7fbb3d484696d723f?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyNzIyMCwiaWF0IjoxNjA2MzIyNDIwLCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXo0OTRjMmNlNmJlOTQyNmQ3ZmJiM2Q0ODQ2OTZkNzIzZiJ9.W59grXU5BH6QyduBAc8Zaacgn2oi1I4mAqg5JRGhSrY&download=image.png "")

**从库配置**

1. 开启relay-log中继日志，配置server-id

修改/etc/my.cnf.

![](https://tcs.teambition.net/storage/311zacd2bcab906102f6a8ce11ca18265184?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyNzQ4OCwiaWF0IjoxNjA2MzIyNjg4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXphY2QyYmNhYjkwNjEwMmY2YThjZTExY2ExODI2NTE4NCJ9.Fk8qBJgjdK307dvfGD9BGZCz9T6QWYYqthJnjZ8-bEA&download=image.png "")

1. 重启slave服务库

1. 登录两台slave服务器，创建链接到master服务器进行同步

1. 对从库进行链接授

![](https://tcs.teambition.net/storage/311zc600e82a4a9f245f4f44db808fb2061e?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyNzYyNCwiaWF0IjoxNjA2MzIyODI0LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXpjNjAwZTgyYTRhOWYyNDVmNGY0NGRiODA4ZmIyMDYxZSJ9.WNxk9R8OaTe5s_6n9EM7goRf5FvZtOyPltJRO9GGxhc&download=image.png "")

### 3. 半同步配置

在从库接收到binlog日志之后给master发送ack消息，主库接收到ack消息之后，进行commit操作。（半同步操作需要安装sime插件）

### 4. MHA安装

1. 四台服务器都需要安装mha4mysql-node。MHA的node依赖于perl-dbd-mysql,所以需要先安装perl-dbd-mysql

1. 在MHA Mansger服务器安装mha4mysql-manager。

1. 配置mha

    - 初始化配置目录

![](https://tcs.teambition.net/storage/311zc7bed24f08893961b9b557a7224e4cfc?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyODQxMSwiaWF0IjoxNjA2MzIzNjExLCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXpjN2JlZDI0ZjA4ODkzOTYxYjliNTU3YTcyMjRlNGNmYyJ9.au2B3ZQODr-l0jU2wLse1-zQ4ykyoEpDVzDtE9W8-Bw&download=image.png "")

    - 配置监控全局配置文件 /etc/masterha_default.cnf

![](https://tcs.teambition.net/storage/311z5f73841a8e96cc190064d698b28bb363?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyODQ4MSwiaWF0IjoxNjA2MzIzNjgxLCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXo1ZjczODQxYThlOTZjYzE5MDA2NGQ2OThiMjhiYjM2MyJ9.tsQJQNw2FdP01uW8JYGLUhUIYSosJX3bmHgTJ4-yPsw&download=image.png "")

    - 配置监控实例配置文件 /etc/mha/app1.cnf

![](https://tcs.teambition.net/storage/311zc7f988ec8acd832a894a224d192be48f?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyODU4NywiaWF0IjoxNjA2MzIzNzg3LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXpjN2Y5ODhlYzhhY2Q4MzJhODk0YTIyNGQxOTJiZTQ4ZiJ9.QY4Pn6_REbsiMZmXbXTe3jV0Qoaf0DMZJIWSWaUWEEk&download=image.png "")

- 启动MHA Manager

![](https://tcs.teambition.net/storage/311z881f0de15f9f12d06e4cbaf232c3f451?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyODY2NiwiaWF0IjoxNjA2MzIzODY2LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXo4ODFmMGRlMTVmOWYxMmQwNmU0Y2JhZjIzMmMzZjQ1MSJ9.I7vmRpYpWd6jNwKeNJvxpa59Y3syLkAcWpzZYBl8DvE&download=image.png "")

## 三. 验证

### 半同步复制测试

    1. 对主库添加一条数据

![](https://tcs.teambition.net/storage/311zb2b1ca14c208c9a5edc5de6cfaec4222?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyNzk5OSwiaWF0IjoxNjA2MzIzMTk5LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXpiMmIxY2ExNGMyMDhjOWE1ZWRjNWRlNmNmYWVjNDIyMiJ9.yybWvETC9EFO2ZJeOTifr3L5sI2S0OAkEgVSZmoENtQ&download=image.png "")

1. 查看从库是否同步

![](https://tcs.teambition.net/storage/311z636b62cd6090d1f9faedfe3bd102c1c9?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyODAzOCwiaWF0IjoxNjA2MzIzMjM4LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXo2MzZiNjJjZDYwOTBkMWY5ZmFlZGZlM2JkMTAyYzFjOSJ9._kidhCKwerpi_VhAh9GZknjOnzf0NtnmXQoh0VSiWp8&download=image.png "")



### MHA故障转移测试

1. 模拟主节点崩溃：msater上停止mysql服务。 

```sql
systemctl stop mysql
```

1. 在MHA Manager上查看MHA服务和切换日志

![](https://tcs.teambition.net/storage/311z3fe3f16deb4e2533e29dfd8eeec413f2?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyOTIxNSwiaWF0IjoxNjA2MzI0NDE1LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXozZmUzZjE2ZGViNGUyNTMzZTI5ZGZkOGVlZWM0MTNmMiJ9.YlXp9CCHkhoX28zQSm0pfjFPiUsPcTzoBpvpIh7sdh4&download=image.png "")

1. 切换后主节点机器状态

![](https://tcs.teambition.net/storage/311z087cf7c25907ae3897335356483ac771?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyOTg0NiwiaWF0IjoxNjA2MzI1MDQ2LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXowODdjZjdjMjU5MDdhZTM4OTczMzUzNTY0ODNhYzc3MSJ9.QBnXZJnNPwXNyHIZyJotRoHNRPpjJMy1elUsd_Fxl3U&download=image.png "")

1. 切换后主节点添加数据，和从库数据同步情况

![](https://tcs.teambition.net/storage/311z4826f12030eb7967ab8b2794f1fae54c?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyOTcwMiwiaWF0IjoxNjA2MzI0OTAyLCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXo0ODI2ZjEyMDMwZWI3OTY3YWI4YjI3OTRmMWZhZTU0YyJ9.9LZt6jBH49SiVgEno2SJu9OB86Hqyss1OcZGznHBbkA&download=image.png "")

![](https://tcs.teambition.net/storage/311za87dd84bba6611f678b68689e68231b2?Signature=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJBcHBJRCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9hcHBJZCI6IjU5Mzc3MGZmODM5NjMyMDAyZTAzNThmMSIsIl9vcmdhbml6YXRpb25JZCI6IjVmNTA1N2ZkMzcxYmI1MmYyNWRlMjNiMiIsImV4cCI6MTYwNjkyOTc0NCwiaWF0IjoxNjA2MzI0OTQ0LCJyZXNvdXJjZSI6Ii9zdG9yYWdlLzMxMXphODdkZDg0YmJhNjYxMWY2NzhiNjg2ODllNjgyMzFiMiJ9.G7xCcg5m__toQw90oXl1jHTHJ30gMHyFubyBZVaG7U0&download=image.png "")

